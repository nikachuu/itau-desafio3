import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IFinancialPostings } from '../models/financial-postings.interface';

@Injectable({
    providedIn: 'root'
})
export class FinancialPostingsService {
    private _URL: string = 'https://desafio-it-server.herokuapp.com';

    constructor(
        private _http: HttpClient
    ) { }

    public GetFinancialPostings(): Observable<HttpResponse<IFinancialPostings[]>> {
        return this._http.get<IFinancialPostings[]>(`${this._URL}/lancamentos`, { observe: 'response'});    
    }
}