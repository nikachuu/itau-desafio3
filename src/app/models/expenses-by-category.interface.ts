export interface IExpensesByCategory {
    category: number;
    totalValue: number;
}