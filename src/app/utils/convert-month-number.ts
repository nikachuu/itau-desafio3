import { EMonths } from '../enums/months.enum';

export function convertMonthNumber(number: number): string {
    return EMonths[number];
}