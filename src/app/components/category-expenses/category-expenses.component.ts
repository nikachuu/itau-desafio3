import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { convertCategoryNumber } from 'src/app/utils/convert-category-number';
import { selectGeneralExpenses } from 'src/app/store/selectors/general-expenses.selectors';
import { IFinancialPostings } from 'src/app/models/financial-postings.interface';
import { GetGeneralExpenses } from 'src/app/store/actions/general-expenses.actions';
import { IExpensesByCategory } from 'src/app/models/expenses-by-category.interface';

@Component({
  selector: 'app-category-expenses',
  templateUrl: './category-expenses.component.html',
  styleUrls: ['./category-expenses.component.scss']
})
export class CategoryExpensesComponent implements OnInit {

  selectGeneralExpenses$ = this._store.pipe(select(selectGeneralExpenses));
  categoryExpenses: IExpensesByCategory[];

  convertCategoryNumber = convertCategoryNumber;
  
  constructor(
    private _store: Store<IAppState>
  ) { }

  ngOnInit(): void {
    this.selectGeneralExpenses$.subscribe((generalExpenses: IFinancialPostings[]) => {

      if (generalExpenses == null) {
        this._store.dispatch(new GetGeneralExpenses())
      } else { 

        const getExpenseByCategory = (prev: any, curr: any) => {
          return prev + curr;
        }

        const validCategories = generalExpenses.reduce((prev, curr) => {
            return !prev.includes(curr.categoria) ? prev.concat(curr.categoria) : prev;
        }, []);

        const categExpenses = validCategories.map(categ => (
        {
          category: categ,
          totalValue: generalExpenses.filter(m => m.categoria == categ).map(m => m.valor).reduce(getExpenseByCategory, 0)
        }));
        
        this.categoryExpenses = categExpenses;
      }     
    });
  }
}
