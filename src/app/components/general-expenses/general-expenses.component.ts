import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { GetGeneralExpenses } from 'src/app/store/actions/general-expenses.actions';
import { selectGeneralExpenses } from 'src/app/store/selectors/general-expenses.selectors';
import { IFinancialPostings } from 'src/app/models/financial-postings.interface';
import { convertCategoryNumber } from '../../utils/convert-category-number';
import { convertMonthNumber } from '../../utils/convert-month-number';

@Component({
  selector: 'app-general-expenses',
  templateUrl: './general-expenses.component.html',
  styleUrls: ['./general-expenses.component.scss']
})
export class GeneralExpensesComponent implements OnInit {

  generalExpenses: IFinancialPostings[];

  selectGeneralExpenses$ = this._store.pipe(select(selectGeneralExpenses));

  convertCategoryNumber = convertCategoryNumber;
  convertMonthNumber = convertMonthNumber;

  constructor(
    private _store: Store<IAppState>
  ) { }

  ngOnInit(): void {
    this._store.dispatch(new GetGeneralExpenses());
    this.selectGeneralExpenses$.subscribe(response => {
      if (response == null) {
        this._store.dispatch(new GetGeneralExpenses());
      } else {
        this.generalExpenses = response;
      }
    });
  }
}
