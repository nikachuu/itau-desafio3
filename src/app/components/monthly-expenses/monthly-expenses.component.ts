import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { selectGeneralExpenses } from 'src/app/store/selectors/general-expenses.selectors';
import { GetGeneralExpenses } from 'src/app/store/actions/general-expenses.actions';
import { IFinancialPostings } from 'src/app/models/financial-postings.interface';
import { IExpensesByMonth } from 'src/app/models/expenses-by-month.interface';
import { convertCategoryNumber } from '../../utils/convert-category-number';
import { convertMonthNumber } from '../../utils/convert-month-number';

@Component({
  selector: 'app-monthly-expenses',
  templateUrl: './monthly-expenses.component.html',
  styleUrls: ['./monthly-expenses.component.scss']
})
export class MonthlyExpensesComponent implements OnInit {

  constructor(
    private _store: Store<IAppState>
  ) { }

  selectGeneralExpenses$ = this._store.pipe(select(selectGeneralExpenses));
  monthlyExpenses: IExpensesByMonth[];

  convertCategoryNumber = convertCategoryNumber;
  convertMonthNumber = convertMonthNumber;

  ngOnInit(): void {
    this.selectGeneralExpenses$.subscribe((generalExpenses: IFinancialPostings[]) => {
      if (generalExpenses == null) {
        this._store.dispatch(new GetGeneralExpenses())
      } else {    

        const getExpenseByMonth = (prev: any, curr: any) => {
          return prev + curr;
        }

        const validMonths = generalExpenses.reduce((prev, curr) => {
            return !prev.includes(curr.mes_lancamento) ? prev.concat(curr.mes_lancamento) : prev;
        }, []);

        const sortMonths = (elem1: { month: number; }, elem2: { month: number; }) => {
          if (elem1.month < elem2.month) {
            return -1;
          }
          if (elem1.month > elem2.month) {
            return 1;
          }
          return 0;
        }

        const months = validMonths.map(mes => (
        {
          month: mes,
          totalValue: generalExpenses.filter(m => m.mes_lancamento == mes).map(m => m.valor).reduce(getExpenseByMonth, 0)
        })).sort(sortMonths);
        
        this.monthlyExpenses = months;
      }      
    })
  }
}
