import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { routerAnimation } from 'src/app/utils/route-animation';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [ routerAnimation() ]
})
export class NavbarComponent implements OnInit {

  navLinks = [
    {
      path: '/general-expenses',
      label: 'Lista Geral'    
    }, 
    { 
      path: '/monthly-expenses',
      label: 'Por Mês'
    },
    {
      path: '/category-expenses',
      label: 'Por Categoria'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  public getRouteAnimation(outlet: RouterOutlet) {
    const res =
      outlet.activatedRouteData.num === undefined
        ? -1
        : outlet.activatedRouteData.num;

    return res;
  }
}
