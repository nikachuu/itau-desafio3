# Desafio 3 - Itaú

Olá! :)\
Gostaria muito de agradecer a oportunidade de participar do processo do Itaú!

## Frameworks de escolha 

- **Angular 9**
- **NgRx**, para gerenciamento de Estados/Side Effects *(ambos EXTREMAMENTE importante pra programação funcional!)* e para ter melhor comunicação entre componentes
- Mantive o **Angular Material** como Framework de CSS, como pedido, mas o layout é **completamente** **diferente** do anterior
- **Normalize.css** para 'resetar' comportamento padrão de folhas de estilo

## Arquitetura de projeto
- Reproduzi a arquitetura que estou acostumada a lidar no trabalho:
    - **components**: folder responsável por armazenar cada componente do projeto; normalmente eu também teria uma pasta pages, mas não vi necessidade por se tratar de um projeto pequeno
    - **enums**: armazena os arquivos responsaveis por mapear as Categorias e Meses de Lançamento, para poder converter tudo pra string posteriormente
    - **models**: armazena os arquivos de interface
    - **services**: armazena os serviços; nesse caso, a comunicação com a API se inicia por aqui
    - **store**: onde possui toda a arquitetura do NgRx, com actions, effects, reducers, selectors e state
    - **utils**: armazena funções de lógica pequena e útil (assim como o nome da pasta indica) que são utilizadas por mais de um componente

## Build e deploy

- O projeto foi buildado e deployado nesse [link do netlify](https://alinesantos-itaud3.netlify.com/)
- Como padrão, também pode ser rodado localmente após npm install e rodando `npm start` ou `ng serve --open`

## Considerações gerais

- Aqui foquei em gerar a lógica de configuração dos dados da tabela de forma onde os dados são mais imutaveis e as funções também mais confiáveis - removendo loops e afins

É isso! Chegamos ao fim.\
Aguardando retorno. :)\
\
\
*Aline de Lima Santos*